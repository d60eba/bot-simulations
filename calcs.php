<?php
#Run forever
set_time_limit(0);

#How often do we win?
$WinPercent = 0.45;	

#What percent of stack do we win, when we win? This is a random range between 2.58 to 2.78 (based on past results)
$WinPercentofStack = array(2.58/100,2.78/100);

#What percent of stack do we lose, when we lose? This is a random range between 1.14 to 1.58 (based on past results)
$LosePercentofStack = array(1.14/100,1.58/100);

#How often do we get a win outlier? (i.e a big win) Random range between two numbers
$WinOutlierPercent = array(4,5);	

#How often do we get a lose outlier? (i.e a big loss) Random range between two numbers
$LoseOutlierPercent = array(6,8);	

#When we get a big win how much bigger than standard is it?
$WinOutlierMultiple = array(2,4);	

#When we get a big loss how much bigger than standard is it?
$LoseOutlierMultiple = array(4,6);	

#Starting stack
$StartAmount = 	7000;	

#How much of our stack are we betting with?
$StackPercentBet =	95;	

#Monthly fee?
$MonthlyCost = 200;

#What amount do we consider ruin? (i.e we give up, set to 0 to continue to bust)
$RuinAt = 2000;

if(isset($_GET['csv'])) {
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename=stats.csv");
    header("Pragma: no-cache");
    header("Expires: 0");
}

#Headers frot he csv
$out = "Draw Down %,StackPercentBet,Leverage,num_bust,num_bust %,num_won,num_won %,num_lost,num_lost %,lowest_final_stack,highest_final_stack,average_final_stack,lowest_drawdown,highest_drawdown,average_drawdown\n";
file_put_contents("stats.csv",$out);

#Run for 95, 75 and 50% of stack bets
foreach(array("95","75","50") AS $StackPercentBet) {//,

    #Run for 0%, 2.5% and 5% monthly drawings
    foreach(array(0,2.5,5,10) AS $MonthlyDrawDown) {//

        $MonthlyDrawDown = $MonthlyDrawDown/100;#set as percentage

        #Run for 1-5x leverage
        foreach(array("1","2","3","4","5") As $Leverage) { //

            #Initialise variables
            $num_bust = 0;
            $num_lost = 0;
            $num_won = 0;    
            $count_stack = 0;
            $count_draw_down = 0;       

            #Run for 10,000 iterations
            for($j=0;$j<10000;$j++) {

                #Initialise variables
                $lowest_stack = 0;
                $highest_stack = 0;
                $DrawDown = 0;
                $final_monthly = 0;

                #Set starting stacksize
                $StackSize = $StartAmount;                        

                #Run through each day of the week
                for($i=0;$i<(365*1);$i++) {

                $win_percent = "";
                $lose_percent = "";
                $WinAmount = "";
                $LoseAmount = "";    

                #Get the outlier figures from those set at start of script
                $CurrentWinOutlierPercent = mt_rand($WinOutlierPercent[0],$WinOutlierPercent[1])/100;	
                $CurrentLoseOutlierPercent = mt_rand($LoseOutlierPercent[0],$LoseOutlierPercent[1])/100;	


                    #Random number 1-100                    
                    $rando = (mt_rand(0,100) /100);

                    #Is it a win or a loss based on that random number?
                    $result = (($rando <= $WinPercent) ? "WIN" : "LOSS");

                    #Set another random number to decide outlier or not
                    $rando = (mt_rand(0,100) /100);

                    #If it's win
                    if($result == "WIN") {

                        #Is this an outlier?
                        $outlier_result = (($rando <= $CurrentWinOutlierPercent) ? true : false);

                        #How much are we winning?
                        $win_percent = (mt_rand($WinPercentofStack[0]*1000,$WinPercentofStack[1]*1000)/1000);

                        if($outlier_result) {

                            #If it's an outlier, we win more
                            $win_percent = $win_percent * mt_rand($WinOutlierMultiple[0],$WinOutlierMultiple[1]);

                        }        

                        #Win amount is win percent * stack size * stack % * leverage
                        $WinAmount = $win_percent * $StackSize * ($StackPercentBet/100) * $Leverage;

                        #Add to stack
                        $StackSize += $WinAmount;

                        #echo "STACK " . $StackSize . " WIN " . $WinAmount . " at " . ($win_percent*100) . "(" . $outlier_result .  " ". $win_percent ."*". $StackSize ."*". ($StackPercentBet/100) ."*". $Leverage . "%)\n";

                    #If it's a loss
                    } else {

                        #Is this an outlier?
                        $outlier_result = (($rando <= $CurrentLoseOutlierPercent) ? true : false);

                        #How much are we losing?
                        $lose_percent = (mt_rand($LosePercentofStack[0]*1000,$LosePercentofStack[1]*1000)/1000);

                        if($outlier_result) {

                             #If it's an outlier, we lose more
                            $lose_percent = $lose_percent * mt_rand($LoseOutlierMultiple[0],$LoseOutlierMultiple[1]);	

                        }        

                        #Lose amount is lose percent * stack size * stack % * leverage
                        $LoseAmount = $lose_percent * $StackSize * ($StackPercentBet/100) * $Leverage;

                        #Deduct from stack
                        $StackSize -= $LoseAmount;

                        #echo "STACK " . $StackSize . " LOSE " . $LoseAmount . " at " . ($lose_percent*100) . "(" . $outlier_result . " ". $lose_percent ."*". $StackSize ."*". ($StackPercentBet/100) ."*". $Leverage . "%)\n";


                    }

                    #Every 30 days
                    if($i  % 30 == 0) {

                        #Add monthly cost to final monthly cost
                        $final_monthly += $MonthlyCost;

                        #Add to drawdown the stacksize * monthly draw %
                        $DrawDown += $MonthlyDrawDown * $StackSize;

                        #Remove monthly draw from stack
                        $StackSize -= $MonthlyDrawDown * $StackSize;

                        #A count for the total amount of drawdown
                        $count_draw_down += $MonthlyDrawDown * $StackSize;
                    }

                    #Stop the year if we ruined
                    if($StackSize  < $RuinAt) {
                        #$num_bust++;
                        break;
                    }
                    
                    #Keep a log
                    $log[$i] = $StackSize;// . " |  " . ((float)$lose_percent*100) . " | " . ((float)$win_percent*100);


                }

                //Remove accumulated monthly
                $StackSize -= $final_monthly;

                #echo $StackSize . " "  . $lowest_stack . " " . $highest_stack . " " . $final_monthly . "\n";
                #print_r($log);
                #die();

                #Very first run
                if($j == 0) {

                    #Mark the lowest and highest
                    $lowest_low = $lowest_stack;
                    $highest_final_stack = $StackSize;
                    $lowest_final_stack = $StackSize;
                    $highest_drawdown = $DrawDown;
                    $lowest_drawdown = $DrawDown;
                    
                #Update the lowest and highest    
                } else {

                    if($lowest_stack < $lowest_low) {
                        $lowest_low = $lowest_stack;
                    }

                    if($StackSize > $highest_final_stack) {
                        $highest_final_stack = $StackSize;
                    }

                    if($StackSize < $lowest_final_stack) {
                        $lowest_final_stack = $StackSize;
                    }

                    if($DrawDown < $lowest_drawdown) {
                        $lowest_drawdown = $DrawDown;
                    }

                    if($DrawDown > $highest_drawdown) {
                        $highest_drawdown = $DrawDown;
                    }


                }

                //If our stacksize is less than Ruin, we lost
                if($StackSize  < $RuinAt) {
                    $num_bust++;
                //If final stacksize  lower than our starting stack plus the amount currently taken out, we lost.                    
                } else if($StackSize < ($StartAmount + $DrawDown)) {
                    $num_lost++;
                //If our final stacksize is greater than start amount, we won
                } else if(($StackSize+$DrawDown) >= $StartAmount) {
                    $num_won++;
                }    

    
                #Keep count of total stack size (to calculate average)
                $count_stack += $StackSize;

                echo $StackSize . " " . $StartAmount . " " . $DrawDown . "\n\n";


            }



            #echo "\nBetting at $StackPercentBet % of stack\n";

            
            /*echo "
            Num busts: $num_bust (" . ($num_bust / $j) * 100 . "%)
            Num won: $num_won (" . ($num_won/$j)*100 ."%)
            Num lost: $num_lost (" . ($num_lost/$j)*100 ."%)
            Lowest Final Stack: " . number_format($lowest_final_stack,2) . "
            Highest Final Stack: " . number_format($highest_final_stack,2) . "
            Average Final Stack: " . number_format(($count_stack/$j),2) . "
            Lowest Draw Down: " . number_format($lowest_drawdown,2) . "
            Highest Draw Down " . number_format($highest_drawdown,2) . "
            Average Draw Down " . number_format($count_draw_down/$j,2) . "
            ";*/

            #The run has finished write out results to file

            $out = $MonthlyDrawDown . "," .
            $StackPercentBet . "," . 
            $Leverage . "," . 
            $num_bust . "," .
            ($num_bust / $j) * 100 . "," .
            $num_won . "," . 
            ($num_won/$j)*100  . "," .
            $num_lost . "," .
            ($num_lost/$j)*100 . "," . 
            number_format($lowest_final_stack,2,".","") . "," . 
            number_format($highest_final_stack,2,".","") . "," .
            number_format(($count_stack/$j),2,".","") . "," .
            number_format($lowest_drawdown,2,".","") . "," .
            number_format($highest_drawdown,2,".","") . "," .
            number_format(($count_draw_down/$j),2,".","") . "," . "\n";

            file_put_contents("stats.csv",$out,FILE_APPEND);

            #Write out log for debugging
            foreach($log AS $day=>$stack) {
                #echo $day . "\t" . $stack . "\n";
            }

        }

    }

}

die();